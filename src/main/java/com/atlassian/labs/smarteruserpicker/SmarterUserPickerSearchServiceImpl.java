package com.atlassian.labs.smarteruserpicker;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.NotNull;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.synonym.SolrSynonymParser;
import org.apache.lucene.analysis.synonym.SynonymFilter;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ClassPathResource;

public class SmarterUserPickerSearchServiceImpl implements SmarterUserPickerSearchService, InitializingBean
{

    // TODO: eventually this class should implement com.atlassian.jira.bc.user.search.UserPickerSearchService

    private static final Logger log = LoggerFactory.getLogger(SmarterUserPickerSearchServiceImpl.class);
    private static final String SYNONYMS_SOLR_FILENAME = "synonyms.txt";
    private static final String NAME_FIELD = "name";
    private static final String DISPLAY_NAME_FIELD = "displayName";
    private static final String INITIALS_FIELD = "initials";
    private static final String TWITTER_NAME_FIELD = "twitterName";
    private static final float RELATIVE_THRESHOLD_SCORE = 0.50f;

    private final UserManager userManager;
    private final RAMDirectory directory = new RAMDirectory();
    private final Analyzer analyzer;
    private final SynonymMap synonymMap;
    private int maxSuggestions = 10;

    public SmarterUserPickerSearchServiceImpl()
    {
        // TODO: remove this constructor, use injection
        this((UserManager) ComponentAccessor.getUserManager());
    }

    public SmarterUserPickerSearchServiceImpl(UserManager userManager)
    {
        this.userManager = userManager;
        this.synonymMap = readSynonymMap(SYNONYMS_SOLR_FILENAME);
        // the same analyzer is used for queries and indexing, that means
        // the synonym filter is used for both, which may deliver strange results
        this.analyzer = new Analyzer()
        {
            @Override
            public TokenStream tokenStream(String fieldName, Reader reader)
            {
                return new SynonymFilter(
                    new StopFilter(Version.LUCENE_36,
                                   new LowerCaseFilter(Version.LUCENE_36,
                                                       new StandardFilter(Version.LUCENE_36,
                                                                          new StandardTokenizer(
                                                                              Version.LUCENE_36, reader))),
                                   StopAnalyzer.ENGLISH_STOP_WORDS_SET),
                    synonymMap, true
                );
            }
        };
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        // initialize the index when the component is created
        buildIndex();
    }

    private SynonymMap readSynonymMap(String classpathResource)
    {
        SolrSynonymParser synonymParser = new SolrSynonymParser(true, true, new Analyzer()
        {
            @Override
            public TokenStream tokenStream(String fieldName, Reader reader)
            {
                return new LowerCaseFilter(Version.LUCENE_36,
                                           new StandardFilter(Version.LUCENE_36,
                                                              new StandardTokenizer(Version.LUCENE_36, reader)));
            }
        });
        try
        {
            log.trace("Reading synonym map from file in the classpath {}", classpathResource);
            synonymParser.add(new InputStreamReader(new ClassPathResource(classpathResource,
                                                                          SmarterUserPickerSearchServiceImpl.class).getInputStream()));
            return synonymParser.build();
        }
        catch (IOException e)
        {
            throw new RuntimeException("While reading the synonyms file", e);
        }
        catch (java.text.ParseException e)
        {
            throw new RuntimeException("While reading the synonyms file", e);
        }
    }

    @Override
    public List<User> findUsers(@NotNull String query)
    {
        if (query.trim().isEmpty())
        {
            return Collections.emptyList();
        }
        try
        {
            IndexReader indexReader = IndexReader.open(directory);
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);
            QueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_36,
                                                                new String[] {
                                                                    DISPLAY_NAME_FIELD,
                                                                    NAME_FIELD,
                                                                    INITIALS_FIELD,
                                                                    TWITTER_NAME_FIELD
                                                                },
                                                                analyzer);
            Query luceneQuery = queryParser.parse(query.trim());
            ScoreDoc[] hits = executeQuery(indexSearcher, luceneQuery);
            if (hits.length == 0)
            {
                // fallback to wildcard query
                log.trace("No results for query {} so far, falling back to wildcard query mode", query);
                Query luceneWildcardQuery = queryParser.parse(query.trim() + "*");
                hits = executeQuery(indexSearcher, luceneWildcardQuery);
            }
            if (hits.length == 0)
            {
                // fallback to fuzzy query
                log.trace("No results for query {} so far, falling back to fuzzy query mode", query);
                Query luceneFuzzyQuery = queryParser.parse(query.trim() + "~");
                hits = executeQuery(indexSearcher, luceneFuzzyQuery);
            }
            List<User> users = new LinkedList<User>();
            for (ScoreDoc hit : hits)
            {
                if (hit.score >= hits[0].score * RELATIVE_THRESHOLD_SCORE)
                {
                    Document document = indexReader.document(hit.doc);
                    users.add(userManager.getUser(document.get(NAME_FIELD)));
                }
            }
            return users;
        }
        catch (ParseException e)
        {
            throw new RuntimeException("When finding users by query=" + query, e);
        }
        catch (CorruptIndexException e)
        {
            throw new RuntimeException("When finding users by query=" + query, e);
        }
        catch (IOException e)
        {
            throw new RuntimeException("When finding users by query=" + query, e);
        }
    }

    private ScoreDoc[] executeQuery(IndexSearcher indexSearcher, Query luceneQuery) throws IOException
    {
        log.trace("Querying index with {}", luceneQuery);
        TopScoreDocCollector collector = TopScoreDocCollector.create(maxSuggestions, true);
        indexSearcher.search(luceneQuery, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;
        log.trace("Found {} results", hits.length);
        return hits;
    }

    @Override
    public synchronized void buildIndex()
    {
        log.info("Creating an index of users");
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LUCENE_36, analyzer);
        try
        {
            IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);
            indexWriter.deleteAll(); // wipe the directory clean before indexing
            Set<User> allUsers = userManager.getAllUsers();
            log.info("Indexing {} users", allUsers.size());
            for (User user : allUsers)
            {
                indexUser(indexWriter, user);
            }
            indexWriter.close();
            log.info("User indexing completed, size: {} bytes", directory.sizeInBytes());
        }
        catch (IOException e)
        {
            throw new RuntimeException("When creating index", e);
        }
    }

    private void indexUser(IndexWriter indexWriter, User user) throws IOException
    {
        Document document = new Document();
        document.add(new Field(NAME_FIELD, user.getName(), Field.Store.YES, Field.Index.NOT_ANALYZED));
        document.add(new Field(DISPLAY_NAME_FIELD, user.getDisplayName(), Field.Store.NO, Field.Index.ANALYZED));
        document.add(new Field(INITIALS_FIELD,
                               getUserInitials(user).toLowerCase(),
                               Field.Store.NO,
                               Field.Index.NOT_ANALYZED));
        document.add(new Field(TWITTER_NAME_FIELD,
                               user.getDisplayName().replaceAll(" ", ""),
                               Field.Store.NO,
                               Field.Index.ANALYZED));
        log.trace("Indexing user {}", user);
        indexWriter.addDocument(document);
    }

    @Override
    public String getUserInitials(User user)
    {
        StringBuffer initials = new StringBuffer();
        for (String component : user.getDisplayName().split("[\\s\\-]+"))
        {
            initials.append(component.charAt(0));
        }
        return initials.toString();
    }

    @Override
    public int getMaxSuggestions()
    {
        return maxSuggestions;
    }

    @Override
    public void setMaxSuggestions(int maxSuggestions)
    {
        this.maxSuggestions = maxSuggestions;
    }

}
