package com.atlassian.labs.smarteruserpicker;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.templaterenderer.TemplateRenderer;

public class SearchPanelServlet extends HttpServlet{

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -571974654963340968L;

	private static final Logger log = LoggerFactory.getLogger(SearchPanelServlet.class);

    private TemplateRenderer templateRenderer;
    
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        templateRenderer.render("com/atlassian/labs/smarteruserpicker/search-panel.vm", resp.getWriter());
    }

    public TemplateRenderer getTemplateRenderer() {
		return templateRenderer;
	}

	public void setTemplateRenderer(TemplateRenderer templateRenderer) {
		this.templateRenderer = templateRenderer;
	}

}