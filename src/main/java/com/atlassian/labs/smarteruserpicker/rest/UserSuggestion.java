package com.atlassian.labs.smarteruserpicker.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserSuggestion
{

    @XmlElement(name = "displayName", nillable = false, required = true)
    private String fullname;

    @XmlElement(name = "name", nillable = false, required = true)
    private String username;

    @XmlElement(name = "avatarUrl", nillable = true, required = false)
    private URI avatarUrl;

    @XmlElement(name = "html", nillable = true, required = false)
    private String html;

    public UserSuggestion()
    {
    }

    public UserSuggestion(String username, String fullname)
    {
        this.username = username;
        this.fullname = fullname;
    }

    private UserSuggestion(String username, String fullname, URI avatarUrl, String html)
    {
        this.username = username;
        this.fullname = fullname;
        this.avatarUrl = avatarUrl;
        this.html = html;
    }

    public String getFullname()
    {
        return fullname;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public URI getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(URI avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserSuggestion that = (UserSuggestion) o;

        if (avatarUrl != null ? !avatarUrl.equals(that.avatarUrl) : that.avatarUrl != null) return false;
        if (fullname != null ? !fullname.equals(that.fullname) : that.fullname != null) return false;
        if (html != null ? !html.equals(that.html) : that.html != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = fullname != null ? fullname.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (avatarUrl != null ? avatarUrl.hashCode() : 0);
        result = 31 * result + (html != null ? html.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "UserSuggestion{" +
                "fullname='" + fullname + '\'' +
                ", username='" + username + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", html='" + html + '\'' +
                '}';
    }

    public static class Builder
    {
        private String username;
        private String fullname;
        private URI avatarUrl = null;
        private String html = null;

        public Builder setUsername(String username)
        {
            this.username = username;
            return this;
        }

        public Builder setFullname(String fullname)
        {
            this.fullname = fullname;
            return this;
        }

        public Builder setAvatarUrl(URI avatarUrl)
        {
            this.avatarUrl = avatarUrl;
            return this;
        }

        public Builder setHtml(String html)
        {
            this.html = html;
            return this;
        }

        public UserSuggestion build()
        {
            return new UserSuggestion(username, fullname, avatarUrl, html);
        }
    }
}
