package com.atlassian.labs.smarteruserpicker.rest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.labs.smarteruserpicker.SmarterUserPickerSearchService;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * A resource to find suggestions for users.
 */
@Path("/picker")
public class SmarterUserPickerResource
{

    private final SmarterUserPickerSearchService service;
    private final AvatarService avatarService;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public SmarterUserPickerResource(SmarterUserPickerSearchService service, AvatarService avatarService, JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.service = service;
        this.avatarService = avatarService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getChoices(@QueryParam("query") String query, @QueryParam("avatarSize") String avatarSize)
    {
        if (StringUtils.isNotBlank(query))
        {
            List<User> users = service.findUsers(query);
            List<UserSuggestion> choices = new LinkedList<UserSuggestion>();
            User loggedInUser = jiraAuthenticationContext.getLoggedInUser();
            Avatar.Size parsedAvatarSize = parseAvatarSize(avatarSize);
            for (User user : users)
            {
                UserSuggestion userSuggestion = new UserSuggestion.Builder()
                        .setUsername(user.getName())
                        .setFullname(user.getDisplayName())
                        .setAvatarUrl(avatarService.getAvatarURL(loggedInUser, user.getName(), parsedAvatarSize))
                        .build();
                choices.add(userSuggestion);
            }
            return Response.ok(new SmarterUserPickerResourceModel(choices)).build();
        }
        else
        {
            // return an empty collection of results to allow clients to clear the old results (if any)
            return Response.ok(new SmarterUserPickerResourceModel(Collections.<UserSuggestion>emptyList())).build();
        }
    }

    private Avatar.Size parseAvatarSize(String avatarSize)
    {
        try
        {
            return Avatar.Size.valueOf(avatarSize);
        }
        catch (Exception e)
        {
            return Avatar.Size.SMALL; // default value
        }
    }
}