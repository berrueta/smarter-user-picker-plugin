package com.atlassian.labs.smarteruserpicker.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.LinkedList;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class SmarterUserPickerResourceModel
{

    @XmlElement(name = "users", nillable = false, required = true)
    private Collection<UserSuggestion> choices = new LinkedList<UserSuggestion>();

    @XmlElement(name = "footer")
    private String footer;

    public SmarterUserPickerResourceModel()
    {
    }

    public SmarterUserPickerResourceModel(Collection<UserSuggestion> choices)
    {
        setChoices(choices);
    }

    // this class exposes its status, but that's required by Wink
    public Collection<UserSuggestion> getChoices()
    {
        return choices;
    }

    public void setChoices(Collection<UserSuggestion> choices)
    {
        if (choices == null)
        {
            this.choices = new LinkedList<UserSuggestion>();
        }
        else
        {
            this.choices = choices;
        }
    }

    public String getFooter()
    {
        return footer;
    }

    public void setFooter(String footer)
    {
        this.footer = footer;
    }

}