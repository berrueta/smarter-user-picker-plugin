package com.atlassian.labs.smarteruserpicker;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.user.UserEvent;
import com.atlassian.jira.event.user.UserEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * @see https://developer.atlassian.com/display/JIRADEV/Plugin+Tutorial+-+Writing+JIRA+event+listeners+with+the+atlassian-event+library
 */
public class UserDataChangesListenerImpl implements UserDataChangesListener, InitializingBean, DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(UserDataChangesListenerImpl.class);

    private final EventPublisher eventPublisher;
    private final SmarterUserPickerSearchService smarterUserPickerSearchService;

    public UserDataChangesListenerImpl(EventPublisher eventPublisher, SmarterUserPickerSearchService smarterUserPickerSearchService)
    {
        this.eventPublisher = eventPublisher;
        this.smarterUserPickerSearchService = smarterUserPickerSearchService;
    }

    @EventListener
    public void onUserEvent(UserEvent userEvent)
    {
        if (userEvent.getEventType() == UserEventType.USER_CREATED)
        {
            log.info("User event received, user index will be rebuilt");
            smarterUserPickerSearchService.buildIndex();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        log.info("Registering event publisher listener");
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception
    {
        log.info("Un-registering event publisher listener");
        eventPublisher.unregister(this);
    }

}