package com.atlassian.labs.smarteruserpicker;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.util.NotNull;

import java.util.List;

public interface SmarterUserPickerSearchService
{
    List<User> findUsers(@NotNull String query);

    String getUserInitials(User user);

    int getMaxSuggestions();

    void setMaxSuggestions(int maxSuggestions);

    void buildIndex();
}
