package com.atlassian.labs.smarteruserpicker;

import com.atlassian.jira.event.user.UserEvent;

public interface UserDataChangesListener
{

    public void onUserEvent(UserEvent userEvent);

}