// this file is shared by the gadget and the search panel

function enableSmartAutocompletion($component, baseUrl, renderResultsCallback, avatarSize) {
	if (renderResultsCallback) {
		// full AJS provides its own implementation of the autocomplete component,
		// and its use is discouraged. If a custom callback function is provided,
		// the autocomplete component is completely ignored and a simplistic
		// implementation is used instead
		$component.keyup(function() {
			var request = { term: $component.val(), avatarSize: avatarSize };
			asyncRequestSuggestions(baseUrl)(request, renderResultsCallback);
		});
	}
	else {
		// if no custom callback function is provided, an attempt is made
		// to use jQuery autocomplete component
		$component.autocomplete({
	        source: asyncRequestSuggestions(baseUrl)
	    });
		if ($component.data("autocomplete")) {
			// great, the jQuery autocomplete component is working
			$component.data("autocomplete")._renderItem = function(ul, user) {
                // I've been unable to have the AUI styles correctly applied to the gadget, so the
                // CSS style is added inline by the Javascript code. TODO: fix
                ul.css("list-style", "none"); // see CSS note above
                ul.addClass("aui-list");
                var name = AJS.$("<div>").addClass("yad").text(user.displayName);
		        var contents = AJS.$("<a tabindex='-1'></a>").addClass("aui-list-item-link aui-iconised-link").append(name); // .append(" (").append(item.username).append(")");
                contents.css("padding-left", "25px"); // see CSS note above
                contents.css("background-position", "5px 3px"); // see CSS note above
                contents.css("background-repeat", "no-repeat"); // see CSS note above
                contents.css("display", "block"); // see CSS note above
                contents.css("background-image", "url(" + user.avatarUrl + ")"); // some help from a UI designer would be welcome here
		        return AJS.$("<li role='menuitem'></li>").addClass("aui-list-item").data("item.autocomplete", user).append(contents).appendTo(ul);
		    };
		}
		else {
			// use of Atlassian's custom autocomplete component is discouraged
			alert("Atlassian's custom autocomplete() component is being used and you have not provided a callback function. Autocompletion will not work.");
		}
	}
}

function asyncRequestSuggestions(baseUrl) {
	var serviceUrl = baseUrl + "/rest/smarter-user-picker/1.0/picker.json";
	return function(request, showChoicesCallback) {
	    AJS.$.ajax({
	        url:  serviceUrl,
	        data: { query: request.term, avatarSize: request.avatarSize },
	        dataType: "json",
	        success: function(data) {
	            showChoicesCallback(data.users); // per contract, always call this callback
	        }
	    });
	};
}	

