package com.atlassian.labs.smarteruserpicker.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import it.com.atlassian.gadgets.pages.Gadget;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import java.util.List;

public class SmarterUserPickerGadget extends Gadget
{

    public static final String TITLE = "Smarter User Picker";
    public static final String ID = "gadget-10100"; // this may change

    @ElementBy(id = "smarteruserpicker")
    private PageElement searchField;

    @ElementBy(className = "ui-autocomplete")
    private PageElement results;

    public SmarterUserPickerGadget(String id)
    {
        super(ID);
    }

    public void searchFor(String searchString)
    {
        // the jQuery.autocomplete component does not react on clear(), so we "type" something to have it cleared
        searchField.clear().type(" ").clear();
        driver.waitUntilElementIsNotVisible(By.className("ui-autocomplete"));
        searchField.type(searchString);
        // this will fail if there isn't any result, because in this case jQuery.autocomplete does not display anything
        driver.waitUntilElementIsVisible(By.className("ui-autocomplete"));
    }

    public List<String> getResults()
    {
        return Lists.transform(results.findAll(By.className("yad"), TimeoutType.AJAX_ACTION), new Function<PageElement, String>() {
            @Override
            public String apply(@Nullable PageElement input) {
                return input.getText();
            }
        });
    }
}
