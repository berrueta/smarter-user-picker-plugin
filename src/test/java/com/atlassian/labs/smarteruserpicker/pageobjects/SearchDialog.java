package com.atlassian.labs.smarteruserpicker.pageobjects;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;

public class SearchDialog
{

    @Inject
    AtlassianWebDriver driver;

    @ElementBy(id = "launch-smarter-user-picker-link-dialog")
    PageElement dialog;

    @ElementBy(id = "h-query")
    PageElement searchField;

    @ElementBy(id = "userSearchResults")
    PageElement results;

    @ElementBy(id = "aui-dialog-close")
    PageElement closeButton;

    public void searchUsers(String query)
    {
        searchField.clear().type(query);
        // TODO: wait until the results are back from the server
    }

    public void close()
    {
        closeButton.click();
        driver.waitUntil(new Function<WebDriver,Boolean>(){
            public Boolean apply( WebDriver webDriver) {
                return !isOpen();
            }
        });
    }

    @WaitUntil
    public boolean isOpen()
    {
        return dialog.isPresent();
    }

    public List<String> getResults()
    {
        return Lists.transform(results.findAll(By.tagName("a"), TimeoutType.AJAX_ACTION), new Function<PageElement, String>() {
            @Override
            public String apply(@Nullable PageElement input) {
                return input.getText();
            }
        });
    }
}
