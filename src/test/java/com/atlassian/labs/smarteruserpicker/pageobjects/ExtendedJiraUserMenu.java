package com.atlassian.labs.smarteruserpicker.pageobjects;

import com.atlassian.jira.pageobjects.components.menu.AuiDropdownMenu;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

public class ExtendedJiraUserMenu extends AuiDropdownMenu
{

    @ElementBy(id = "launch-smarter-user-picker-link")
    PageElement findUsersMenuItem;

    public ExtendedJiraUserMenu()
    {
        super(By.id("header-details-user"));
    }

    public SearchDialog findUsers()
    {
        findUsersMenuItem.click();
        return pageBinder.bind(SearchDialog.class);
    }

    @Override
    public ExtendedJiraUserMenu open()
    {
        return (ExtendedJiraUserMenu) super.open();
    }

    @Override
    public boolean isOpen()
    {
        // the implementation in the parent doesn't seem to work well, so we override it with a simplistic implementation
        return findUsersMenuItem.isVisible();
    }

}
