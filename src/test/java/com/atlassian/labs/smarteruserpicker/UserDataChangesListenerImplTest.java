package com.atlassian.labs.smarteruserpicker;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.user.UserEvent;
import com.atlassian.jira.event.user.UserEventType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class UserDataChangesListenerImplTest
{

    @Before
    public void setup()
    {

    }

    @After
    public void tearDown()
    {

    }

    @Test
    public void checkListenerRegistersItself() throws Exception
    {
        EventPublisher eventPublisher = mock(EventPublisher.class);
        SmarterUserPickerSearchService smarterUserPickerSearchService = mock(SmarterUserPickerSearchService.class);
        UserDataChangesListenerImpl testClass = new UserDataChangesListenerImpl(eventPublisher, smarterUserPickerSearchService);

        testClass.afterPropertiesSet();
        verify(eventPublisher).register(testClass);
    }

    @Test
    public void checkListenerUnregistersItself() throws Exception
    {
        EventPublisher eventPublisher = mock(EventPublisher.class);
        SmarterUserPickerSearchService smarterUserPickerSearchService = mock(SmarterUserPickerSearchService.class);
        UserDataChangesListenerImpl testClass = new UserDataChangesListenerImpl(eventPublisher, smarterUserPickerSearchService);

        testClass.destroy();
        verify(eventPublisher).unregister(testClass);
    }

    @Test
    public void checkReindexingOnUserCreated() throws Exception
    {
        EventPublisher eventPublisher = mock(EventPublisher.class);
        SmarterUserPickerSearchService smarterUserPickerSearchService = mock(SmarterUserPickerSearchService.class);
        UserEvent userEvent = mock(UserEvent.class);
        when(userEvent.getEventType()).thenReturn(UserEventType.USER_CREATED);
        UserDataChangesListenerImpl testClass = new UserDataChangesListenerImpl(eventPublisher, smarterUserPickerSearchService);

        testClass.onUserEvent(userEvent);
        verify(smarterUserPickerSearchService).buildIndex();
    }

    @Test
    public void checkDoNothingOnOtherUserEvents() throws Exception
    {
        EventPublisher eventPublisher = mock(EventPublisher.class);
        SmarterUserPickerSearchService smarterUserPickerSearchService = mock(SmarterUserPickerSearchService.class);
        UserEvent userEvent = mock(UserEvent.class);
        when(userEvent.getEventType()).thenReturn(UserEventType.USER_LOGIN);
        UserDataChangesListenerImpl testClass = new UserDataChangesListenerImpl(eventPublisher, smarterUserPickerSearchService);

        testClass.onUserEvent(userEvent);
        verifyNoMoreInteractions(smarterUserPickerSearchService);
    }

}
