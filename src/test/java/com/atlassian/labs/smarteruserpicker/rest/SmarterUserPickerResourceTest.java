package com.atlassian.labs.smarteruserpicker.rest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.labs.smarteruserpicker.SmarterUserPickerSearchService;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SmarterUserPickerResourceTest
{

    private static final String USER1_FULLNAME = "Mike Sullivan";
    private static final String USER1_NAME = "mike";
    private static final URI USER1_AVATAR_URI;

    static
    {
        try
        {
            USER1_AVATAR_URI = new URI("http://example.org/avatar/mike.png");
        }
        catch (URISyntaxException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Mock private SmarterUserPickerSearchService smarterUserPickerSearchService;
    @Mock private User loggedInUser;
    @Mock private User user1;
    @Mock private AvatarService avatarService;
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;

    @Before
    public void setup()
    {
        when(loggedInUser.getName()).thenReturn("loggedInUser");
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(loggedInUser);
    }

    @After
    public void tearDown()
    {

    }

    @Test
    public void checkFindByLongName()
    {
        when(user1.getDisplayName()).thenReturn(USER1_FULLNAME);
        when(user1.getName()).thenReturn(USER1_NAME);
        when(smarterUserPickerSearchService.findUsers("michael")).thenReturn(ImmutableList.of(user1));
        when(avatarService.getAvatarURL(loggedInUser, USER1_NAME, Avatar.Size.LARGE)).thenReturn(USER1_AVATAR_URI);
        SmarterUserPickerResource resource = new SmarterUserPickerResource(smarterUserPickerSearchService, avatarService, jiraAuthenticationContext);
        Response response = resource.getChoices("michael", Avatar.Size.LARGE.toString());
        final SmarterUserPickerResourceModel message = (SmarterUserPickerResourceModel) response.getEntity();

        assertThat(message.getChoices(), hasItem(new UserSuggestion.Builder()
                .setUsername(USER1_NAME)
                .setFullname(USER1_FULLNAME)
                .setAvatarUrl(USER1_AVATAR_URI)
                .build()));
    }

}
