package com.atlassian.labs.smarteruserpicker;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SmarterUserPickerSearchServiceImplTest
{

    final UserManager mockUserManager = mock(UserManager.class);
    final User mockUser1 = mock(User.class);

    @Test
    public void testGetUserInitials()
    {
        when(mockUser1.getDisplayName()).thenReturn("John F. Kennedy");
        SmarterUserPickerSearchService service = new SmarterUserPickerSearchServiceImpl(mockUserManager);
        assertEquals("JFK", service.getUserInitials(mockUser1));
    }

    @Test
    public void testGetUserInitialsOfCompoundName()
    {
        when(mockUser1.getDisplayName()).thenReturn("Jean-Luc Picard");
        SmarterUserPickerSearchService service = new SmarterUserPickerSearchServiceImpl(mockUserManager);
        assertEquals("JLP", service.getUserInitials(mockUser1));
    }

}
