package it.com.atlassian.labs.smarteruserpicker.acceptance;

import com.atlassian.labs.smarteruserpicker.pageobjects.SmarterUserPickerGadget;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.TestedProductFactory;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;

public class GadgetAcceptanceTest
{

    private JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class);

    private SmarterUserPickerGadget gadget;

    @Before
    public void setUp()
    {
         gadget = getGadget();
    }

    @Test
    public void testMultipleSearch()
    {
        gadget.searchFor("adm");
        assertThat(gadget.getResults(), hasItem("admin"));

        gadget.searchFor("kennedy");
        assertThat(gadget.getResults(), hasItems("Robert Kennedy", "John F. Kennedy"));
    }

    @Test
    public void testSearchByInitials()
    {
        gadget.searchFor("jfk");
        assertEquals(ImmutableList.of("John F. Kennedy"), gadget.getResults());
    }

    // we cannot test this, because jQuery.autocomplete does not display "empty" results,
    // see SmarterUserPickerGadget page object
//    @Test
//    public void testInexistentName()
//    {
//        gadget.searchFor("thenamethatshouldnotbeused");
//        assertTrue("There should be no results", gadget.getResults().isEmpty());
//    }

    private SmarterUserPickerGadget getGadget()
    {
        DashboardPage dashboardPage = jira.gotoLoginPage().login("admin", "admin", DashboardPage.class);
        return dashboardPage.gadgets().getGadgetByTitle(SmarterUserPickerGadget.class, SmarterUserPickerGadget.TITLE);
    }

}