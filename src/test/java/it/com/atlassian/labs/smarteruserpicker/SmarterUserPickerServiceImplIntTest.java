package it.com.atlassian.labs.smarteruserpicker;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.labs.smarteruserpicker.SmarterUserPickerSearchService;
import com.atlassian.labs.smarteruserpicker.SmarterUserPickerSearchServiceImpl;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SmarterUserPickerServiceImplIntTest
{

    final UserManager mockUserManager = mock(UserManager.class);
    final User mockUser1 = mock(User.class);
    final User mockUser2 = mock(User.class);
    final User mockUser3 = mock(User.class);

    private SmarterUserPickerSearchService buildService()
    {
        // FIXME: perhaps the class com.atlassian.jira.user.util.MockUserManager can be used instead
        registerUserInMockUserManager(mockUserManager, mockUser1, "rkennedy", "Bob Kennedy");
        registerUserInMockUserManager(mockUserManager, mockUser2, "johnk", "John F. Kennedy");
        registerUserInMockUserManager(mockUserManager, mockUser3, "katfree", "Cathy Freeman");
        when(mockUserManager.getAllUsers()).thenReturn(new HashSet<User>()
        {{
                add(mockUser1);
                add(mockUser2);
                add(mockUser3);
            }});

        SmarterUserPickerSearchService service = new SmarterUserPickerSearchServiceImpl(mockUserManager);
        service.buildIndex(); // force index construction
        return service;
    }

    private void registerUserInMockUserManager(UserManager mockUserManager, User mockUser, String name, String displayName)
    {
        when(mockUser.getDisplayName()).thenReturn(displayName);
        when(mockUser.getName()).thenReturn(name);
        when(mockUser.toString()).thenReturn(displayName);
        when(mockUserManager.getUser(name)).thenReturn(mockUser);
    }

    @Test
    public void testFindUsersEmptyQuery()
    {
        assertEquals(0, buildService().findUsers("").size());
    }

    @Test
    public void testFindUsersByExactName()
    {
        assertThat(buildService().findUsers("John"), hasItem(mockUser2));
    }

    @Test
    public void testFindUsersByExactSurname()
    {
        assertThat(buildService().findUsers("Kennedy"), hasItems(mockUser1, mockUser2));
    }

    @Test
    public void testFindUsersByExactFullNameDiscardsPartialMatches()
    {
        List<User> users = buildService().findUsers("Bob Kennedy");
        assertThat(users, hasItem(mockUser1));
        assertThat(users, not(hasItem(mockUser2)));// but not other "Kennedy"
    }

    @Test
    public void testFindUsersByIncompleteName()
    {
        assertThat(buildService().findUsers("Kenn"), hasItems(mockUser1, mockUser2));
    }

    @Test
    public void testFindUsersByLongName()
    {
        assertThat(buildService().findUsers("Robert"), hasItem(mockUser1));
    }

    @Test
    public void testFindUsersByShortName()
    {
        assertThat(buildService().findUsers("Jack"), hasItem(mockUser2));
    }

    @Test
    public void testFindUsersByMisspelledName()
    {
        assertThat(buildService().findUsers("Jhon"), hasItem(mockUser2));
    }

// This feature is not implemented yet
//    @Test
//    public void testFindUsersByMisspelledShortName()
//    {
//        assertThat(service.findUsers("Jakc"), hasItem(mockUser2));
//    }

    @Test
    public void testFindUsersByUsername()
    {
        assertThat(buildService().findUsers("katfree"), hasItem(mockUser3));
    }

    @Test
    public void testFindUsersByInitials()
    {
        assertThat(buildService().findUsers("jfk"), hasItem(mockUser2));
    }

    @Test
    public void testFindUsersNoMatch()
    {
        assertTrue(buildService().findUsers("Unlikelyname").isEmpty());
    }

}
