package it.com.atlassian.labs.smarteruserpicker.acceptance;

import com.atlassian.labs.smarteruserpicker.pageobjects.ExtendedJiraUserMenu;
import com.atlassian.labs.smarteruserpicker.pageobjects.SearchDialog;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.TestedProductFactory;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;

public class SearchDialogAcceptanceTest
{

    private JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class);

    private SearchDialog searchDialog;

    @Before
    public void openDialog()
    {
        searchDialog = getSearchDialog();
    }

    @After
    public void closeDialog()
    {
        searchDialog.close();
    }

    @Test
    public void searchByPrefix()
    {
        searchDialog.searchUsers("adm");
        assertThat(searchDialog.getResults(), hasItem("admin"));
    }

    @Test
    public void searchByInexactName()
    {
        searchDialog.searchUsers("kenedy");
        assertThat(searchDialog.getResults(), hasItems("John F. Kennedy", "Robert Kennedy"));
    }

    @Test
    public void searchByExactNameDeliversJustOneResult()
    {
        searchDialog.searchUsers("robert kennedy");
        assertEquals(ImmutableList.of("Robert Kennedy"), searchDialog.getResults());
    }

    @Test
    public void searchByInitials()
    {
        searchDialog.searchUsers("jfk");
        assertThat(searchDialog.getResults(), hasItem("John F. Kennedy"));
    }

    @Test
    public void searchInexistentName()
    {
        searchDialog.searchUsers("thenamethatshouldnotbeused");
        assertTrue("No results should be shown", searchDialog.getResults().isEmpty());
    }

    private SearchDialog getSearchDialog()
    {
        jira.gotoLoginPage().login("admin", "admin", DashboardPage.class);
        ExtendedJiraUserMenu userMenu = jira.getPageBinder().bind(ExtendedJiraUserMenu.class);
        return userMenu.open().findUsers();
    }

}
