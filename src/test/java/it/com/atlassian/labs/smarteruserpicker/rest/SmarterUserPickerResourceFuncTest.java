package it.com.atlassian.labs.smarteruserpicker.rest;

import com.atlassian.labs.smarteruserpicker.rest.SmarterUserPickerResourceModel;
import com.atlassian.labs.smarteruserpicker.rest.UserSuggestion;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.apache.wink.client.handlers.BasicAuthSecurityHandler;
import org.apache.wink.common.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

public class SmarterUserPickerResourceFuncTest
{

    private static final Logger log = LoggerFactory.getLogger(SmarterUserPickerResourceFuncTest.class);

    private final String baseUrl = System.getProperty("baseurl");
    private final String resourceUrl = baseUrl + "/rest/smarter-user-picker/1.0/picker";
    private RestClient client;

    @Before
    public void setup()
    {
        ClientConfig config = new ClientConfig();
        BasicAuthSecurityHandler basicAuthHandler = new BasicAuthSecurityHandler();
        basicAuthHandler.setUserName("admin");
        basicAuthHandler.setPassword("admin");
        config.handlers(basicAuthHandler);
        this.client = new RestClient(config);
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testLoginIsRequired()
    {
        RestClient unauthenticatedClient = new RestClient();
        Resource resource = unauthenticatedClient.resource(resourceUrl).queryParam("query", "michael");
        assertThat(resource.get().getStatusCode(), is(HttpStatus.UNAUTHORIZED.getCode())); // FIXME
    }

    @Test
    public void checkFindByFullname() throws Exception
    {
        Resource resource = client.resource(resourceUrl).queryParam("query", "michael");
        SmarterUserPickerResourceModel message = resource.get(SmarterUserPickerResourceModel.class);
        UserSuggestion userSuggestion = new UserSuggestion.Builder()
                .setUsername("campoviejo")
                .setFullname("Mike Oldfield")
                .setAvatarUrl(new URI("/jira/secure/useravatar?size=small&avatarId=10122"))
                .build();
        assertThat(message.getChoices(), hasItem(userSuggestion));
    }

    @Test
    public void checkFindByShortname() throws Exception
    {
        Resource resource = client.resource(resourceUrl).queryParam("query", "bobby");
        SmarterUserPickerResourceModel message = resource.get(SmarterUserPickerResourceModel.class);
        UserSuggestion userSuggestion = new UserSuggestion.Builder()
                .setUsername("user2")
                .setFullname("Robert Kennedy")
                .setAvatarUrl(new URI("/jira/secure/useravatar?size=small&avatarId=10122"))
                .build();
        assertThat(message.getChoices(), hasItem(userSuggestion));
    }

    @Test
    public void checkFindByInitials() throws Exception
    {
        Resource resource = client.resource(resourceUrl).queryParam("query", "jfk");
        SmarterUserPickerResourceModel message = resource.get(SmarterUserPickerResourceModel.class);
        UserSuggestion userSuggestion = new UserSuggestion.Builder()
                .setUsername("user1")
                .setFullname("John F. Kennedy")
                .setAvatarUrl(new URI("/jira/secure/useravatar?size=small&avatarId=10122"))
                .build();
        assertThat(message.getChoices(), hasItem(userSuggestion));
    }

    @Test
    public void checkNoSuggestions()
    {
        Resource resource = client.resource(resourceUrl).queryParam("query", "noonewilleverbenamedlikethis");
        SmarterUserPickerResourceModel message = resource.get(SmarterUserPickerResourceModel.class);
        assertThat(message.getChoices().size(), is(0));
    }

    @Test
    public void checkEmptyQuery()
    {
        Resource resource = client.resource(resourceUrl).queryParam("query", "");
        SmarterUserPickerResourceModel message = resource.get(SmarterUserPickerResourceModel.class);
        assertThat(message.getChoices().size(), is(0));
    }
}
